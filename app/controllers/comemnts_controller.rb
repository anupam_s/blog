class ComemntsController < ApplicationController
  before_action :set_comemnt, only: [:show, :edit, :update, :destroy]

  # GET /comemnts
  # GET /comemnts.json
  def index
    @comemnts = Comemnt.all
  end

  # GET /comemnts/1
  # GET /comemnts/1.json
  def show
  end

  # GET /comemnts/new
  def new
    @comemnt = Comemnt.new
  end

  # GET /comemnts/1/edit
  def edit
  end

  # POST /comemnts
  # POST /comemnts.json
  def create
    @comemnt = Comemnt.new(comemnt_params)

    respond_to do |format|
      if @comemnt.save
        format.html { redirect_to @comemnt, notice: 'Comemnt was successfully created.' }
        format.json { render :show, status: :created, location: @comemnt }
      else
        format.html { render :new }
        format.json { render json: @comemnt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comemnts/1
  # PATCH/PUT /comemnts/1.json
  def update
    respond_to do |format|
      if @comemnt.update(comemnt_params)
        format.html { redirect_to @comemnt, notice: 'Comemnt was successfully updated.' }
        format.json { render :show, status: :ok, location: @comemnt }
      else
        format.html { render :edit }
        format.json { render json: @comemnt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comemnts/1
  # DELETE /comemnts/1.json
  def destroy
    @comemnt.destroy
    respond_to do |format|
      format.html { redirect_to comemnts_url, notice: 'Comemnt was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comemnt
      @comemnt = Comemnt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comemnt_params
      params.require(:comemnt).permit(:post_id, :body)
    end
end
