require 'test_helper'

class ComemntsControllerTest < ActionController::TestCase
  setup do
    @comemnt = comemnts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:comemnts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create comemnt" do
    assert_difference('Comemnt.count') do
      post :create, comemnt: { body: @comemnt.body, post_id: @comemnt.post_id }
    end

    assert_redirected_to comemnt_path(assigns(:comemnt))
  end

  test "should show comemnt" do
    get :show, id: @comemnt
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @comemnt
    assert_response :success
  end

  test "should update comemnt" do
    patch :update, id: @comemnt, comemnt: { body: @comemnt.body, post_id: @comemnt.post_id }
    assert_redirected_to comemnt_path(assigns(:comemnt))
  end

  test "should destroy comemnt" do
    assert_difference('Comemnt.count', -1) do
      delete :destroy, id: @comemnt
    end

    assert_redirected_to comemnts_path
  end
end
